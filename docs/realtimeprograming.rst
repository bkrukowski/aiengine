Injecting code on the engine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One of the cool features of the engine is the ability to change the behavior while is executing.
The best way to understand this feature is by having a proper example.
We load the library and create a StackLan object with some memory requirements.

.. code:: python

  import pyaiengine

  s = pyaiengine.StackLan()

  s.tcp_flows = 32768
  s.udp_flows = 56384

Just for the example we are going to create 3 DNS rules for handling queries.

.. code:: python

  d1 = pyaiengine.DomainName("Generic net queries",".net")
  d2 = pyaiengine.DomainName("Generic com queries",".com")
  d3 = pyaiengine.DomainName("Generic org queries",".org")

  dm = pyaiengine.DomainManager()

  """ Add the DomainName objects to the manager """
  dm.add_domain_name(d1)
  dm.add_domain_name(d2)
  dm.add_domain_name(d3)

  st.set_domain_name_manager(dm,"DNSProtocol")

Now we open a new context of a PacketDispatcher and enable the shell for interacting with the engine.

.. code:: python

  with pyaiengine.PacketDispatcher("enp0s25") as pd:
      pd.stack = st
      """ We enable the shell for interact with the engine """
      pd.enable_shell = True
      pd.run()

If we execute this code we will see the following messages.

.. code:: bash

  [luis@localhost ai]$ python example.py
  [09/30/16 21:48:41] Lan network stack ready.
  AIEngine 1.6 shell
  [09/30/16 21:48:41] Processing packets from device enp0s25
  [09/30/16 21:48:41] Stack 'Lan network stack' using 51 MBytes of memory

  >>>

Now we are under control of the internal shell of the engine and we can access to the different
components.

.. code:: bash

  >>> print(dm)
  DomainNameManager (Generic Domain Name Manager)
          Name:Generic net queries      Domain:.net     Matchs:10
          Name:Generic org queries      Domain:.org     Matchs:0
          Name:Generic com queries      Domain:.com     Matchs:21

  >>>

And now we inject a callback function for one of the given domains.

.. code:: bash

  >>> def my_callback(flow):
  ...   d = flow.dns_info
  ...   if (d):
  ...     print(str(d))
  ...
  >>> d3.callback = my_callback
  >>>


And wait for domains that ends on .org

.. code:: bash

  >>>  Domain:www.gnu.org

also verify the rest of the components

.. code:: bash

  >>> print(d2)
  Name:Generic org queries      Domain:.org     Matchs:1        Callback:<function my_callback 0x023ffeea378>
  >>> print(dm)
  DomainNameManager (Generic Domain Name Manager)
          Name:Generic net queries      Domain:.net     Matchs:14
          Name:Generic org queries      Domain:.org     Matchs:1        Callback:<function my_callback 0x023ffeea378>
          Name:Generic com queries      Domain:.com     Matchs:21


